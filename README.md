# Name Age - React App
A simple React App that allows users to add entries of data with name and age as attributes per entry. Input validation is also included that displays a modal.

## Setup

```
npm install
npm run start
```
